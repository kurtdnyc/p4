#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <iostream>
#include <set>
#include <vector>
#include <strings.h>
#include <ctype.h>
#include <stdio.h>

using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

void funsort(vector<char> &V)
{
    //compare V[i] to V[i+1] and swap if greater
    char temp;
    for (int i = 0; i < V.size(); ++i)
    {
        for (int j= i +1; j<V.size(); ++j)
        {
            if (V[i] > V[j])
            {
                temp  = V[i];
                V[i] = V[j];
                V[j] = temp;
            }
        }
    }
}

void lowercase(vector<char> &V)
{
    char low;
    int i = 0;
    while (V[i])
    {
        low = V[i];
        V[i] = (tolower(low));
        i++;
    }
}

void check(vector<char> &V)
{
    for (int i = 0; i < V.size(); ++i)
    {
        for (int j= i +1; j<V.size(); ++j)
        {
            if (V[i] == V[j])
            {
                V.erase (V.begin() + j);
                --j;
            }
        }
    }
}
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */

int main(int argc, char *argv[])
{
    // define long options
    static int descending=0, ignorecase=0, unique=0;
    static struct option long_opts[] =
    {
        {"reverse",       no_argument,   0, 'r'},
        {"ignore-case",   no_argument,   0, 'f'},
        {"unique",        no_argument,   0, 'u'},
        {"help",          no_argument,   0, 'h'},
        {0,0,0,0}
    };
    // process options:
    char c;
    int opt_index = 0;
    while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1)
    {
        switch (c)
        {
            case 'r':
                descending = 1;
                break;
            case 'f':
                ignorecase = 1;
                break;
            case 'u':
                unique = 1;
                break;
            case 'h':
                printf(usage,argv[0]);
                return 0;
            case '?':
                printf(usage,argv[0]);
                return 1;
        }
    }
    vector<char> V;
    char a;
    while (fread(&a,1,1,stdin))
    {
        V.push_back(a);
    }
    
    if (ignorecase == 1)
    {
        lowercase(V);
        //convert the char's to lowercase
        funsort(V);
    }
    
    
    funsort(V);
    
    if (unique == 1)
    {
        check(V);
    }
    
    if ((unique ==1) && (ignorecase ==1))
    {
        lowercase(V);
        check(V);
    }
    
    
    if (descending == 1)
    {
        //just iterate backwards lol
        for (int i=V.size(); i != -1 ; --i)
        {
            cout << V[i] << endl;
        }
        return 0;
    }
    
    for (int i=0; i < V.size(); ++i)
    {
        cout << V[i] << endl;
    }
    return 0;
}
